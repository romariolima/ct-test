import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import BrowserSyncPlugin from 'browser-sync-webpack-plugin';

import loaders from './loaders';

const HOST = 'localhost';
const PORT = 3000;
const DEV_SERVER_PORT = PORT + 1;

const config = {
  entry: [
    `webpack-dev-server/client?http://${HOST}:${DEV_SERVER_PORT}`,
    path.resolve(__dirname, '../src/app.js')
  ],
  devtool: 'cheap-module-source-map',
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'app.bundle.js'
  },
  resolve: {
    root: path.resolve(__dirname, '../src'),
    extensions: ['', '.js', '.json']
  },
  module: { loaders },
  devServer: {
    contentBase: path.resolve(__dirname, '../dist'),
    noInfo: false,
    inline: true,
    port: DEV_SERVER_PORT,
    host: HOST
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '../src/index.html')
    }),
    new BrowserSyncPlugin({
      host: HOST,
      port: PORT,
      proxy: `http://${HOST}:${DEV_SERVER_PORT}`
    }, {
      reload: false
    })
  ]
};

export default config;
