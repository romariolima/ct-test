const loaders = [{
  test: /\.js$/,
  exclude: /(node_modules)/,
  loader: 'ng-annotate!babel'
}, {
  test: /\.scss$/,
  loaders: [
    'style',
    'css?sourceMap',
    'sass?sourceMap'
  ]
}, {
  test: /\.html$/,
  loader: 'raw'
}, {
  test: /\.json$/,
  exclude: /node_modules/,
  loader: 'json'
}];

export default loaders;
