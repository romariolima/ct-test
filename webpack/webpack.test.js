import { ProvidePlugin } from 'webpack';
import path from 'path';
import loaders from './loaders';

const providePlugins = new ProvidePlugin({
  $: 'jquery',
  jQuery: 'jquery',
  'window.jQuery': 'jquery',
  'windows.jQuery': 'jquery',
});

const config = {
  devtool: 'inline-source-map',
  resolve: {
    root: path.resolve(__dirname, '../src'),
    extensions: ['', '.js', '.json']
  },
  module: {
    loaders,
    preLoaders: [{
      test: /\.js$/,
      exclude: [
        /.spec.js$/,
        /node_modules/
      ],
      loader: 'isparta'
    }]
  },
  plugins: [providePlugins]
};

export default config;
