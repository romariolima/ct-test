/* @ngInject */
export default class HomeController {
  constructor($state, $filter, carsFactory) {
    this.carsFactory = carsFactory;
    this.$filter = $filter;
    this.$state = $state;
    this.VehAvailRSCore = {
      VehRentalCore: {},
      VehVendorAvails: []
    };
    this.filteredCars = [];

    this.carListFilter = { vendor: '' };

    this.loadAvailableCars();
  }

  loadAvailableCars() {
    return this.carsFactory
    .get({ type: 'available' })
    .then((data) => {
      const { VehRentalCore, VehVendorAvails } = data[0].VehAvailRSCore;
      this.VehAvailRSCore = { VehRentalCore, VehVendorAvails };
      this.setFilteredAvailableCars();
    });
  }

  setFilteredAvailableCars() {
    const {
      $filter,
      VehAvailRSCore: { VehVendorAvails }
    } = this;

    this.filteredCars = $filter('filter')(
      VehVendorAvails, {
        Vendor: { '@Name': this.carListFilter.vendor }
      }
    );
  }

  onSelectVehicle(vehicle) {
    this.$state.go('vehicle', {
      id: `${vehicle.Vendor['@Code']}-${vehicle.Vehicle['@Code']}`
    });
  }
}
