import angular from 'angular';
import uirouter from 'angular-ui-router';
import carsList from 'modules/carsList';

import configRoutes from './configRoutes';

import './home.scss';

export default angular
.module('app.homeFeed', [uirouter, carsList])
.config(configRoutes)
.name;
