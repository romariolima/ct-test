import carsListHtml from './carsList.html';

import './carsList.scss';

class CarsListController {
  constructor() {
    this.availableVehicles = [];
  }

  setVehicles() {
    const vehicles = this.vendorCars
    .reduce((previous, vendor) => {
      const vendorVehicles = vendor.VehAvails
      .map(vehicle =>
        Object.assign(vehicle, { Vendor: vendor.Vendor })
      );

      return previous.concat(vendorVehicles);
    }, []);

    this.availableVehicles = vehicles;
  }

  $onChanges() {
    this.setVehicles();
  }
}

export default () => ({
  template: carsListHtml,
  controller: CarsListController,
  bindings: {
    vendorCars: '<',
    onSelectVehicle: '&'
  }
});
