/* @ngInject */
export default $http => ({
  get({ type = null } = {}) {
    return $http({
      url: 'http://private-e2756-cttest2.apiary-mock.com/cars',
      params: { type }
    })
    .then(data => data.data);
  }
});
