import webpackConfig from './webpack/webpack.test';

export default config =>
  config.set({
    basePath: '',
    frameworks: ['mocha', 'sinon', 'chai'],
    files: ['./test/index.js'],
    webpack: webpackConfig,
    exclude: [],
    preprocessors: {
      './test/index.js': ['webpack', 'sourcemap']
    },
    reporters: ['mocha', 'coverage'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    browsers: ['Chrome'],
    concurrency: Infinity,
    coverageReporter: {
      dir: 'coverage/',
      reporters: [
        { type: 'text-summary' },
        { type: 'html' }
      ]
    },
  });
