import carsList from 'modules/carsList';
import cars from '../../../../test/cars.json';


describe('carsList - Component', () => {
  const firstVendor = cars[0].VehAvailRSCore.VehVendorAvails[0];
  const firstCar = firstVendor.VehAvails[0];
  let $compile;
  let $rootScope;
  let scope;
  let element;

  beforeEach(() => {
    angular.mock.module(carsList);
    angular.mock.inject((_$rootScope_, _$compile_) => {
      $compile = _$compile_;
      $rootScope = _$rootScope_;

      scope = $rootScope.$new();
      scope.cars = cars[0].VehAvailRSCore.VehVendorAvails;
    });
  });

  describe('#render', () => {
    beforeEach(() => {
      angular.mock.inject((_$rootScope_, _$compile_) => {
        element = angular.element('<cars-list vendor-cars="cars" />');
        $compile(element)(scope);
        $rootScope.$digest();
      });
    });

    it('render car name', () => {
      const text = element.find('.car-card').eq(0)
      .find('.car-card__name')
      .text()
      .trim();

      expect(text).to.equal(firstCar.Vehicle.VehMakeModel['@Name']);
    });

    it('render supplier name', () => {
      const text = element.find('.car-card')
      .eq(0)
      .find('.car-card__attributes__supplier')
      .text()
      .trim();

      expect(text).to.contains(firstVendor.Vendor['@Name']);
    });
  });

  describe('#behavior', () => {
    it('execute onSelectVehicle with Vehicle as arg when click on select button', () => {
      scope.onSelectVehicle = sinon.spy();
      element = angular.element('<cars-list vendor-cars="cars" on-select-vehicle="onSelectVehicle(vehicle)" />');
      $compile(element)(scope);
      $rootScope.$digest();

      element.find('.car-card')
      .eq(0)
      .find('.car-card__select__button')
      .triggerHandler('click');

      sinon.assert.calledWith(scope.onSelectVehicle, firstCar);

    });
  })
});
