import VehicleController from './controller/VehicleController';
import vehicleHtml from './view/vehicle.html';

/* @ngInject */
const vehicleResolve = ($stateParams, carsFactory) => {
  const id = $stateParams.id.split('-');
  const vendorId = id[0];
  const carId = id[1];

  return carsFactory.get()
  .then((data) => {
    const vendor = data[0].VehAvailRSCore
    .VehVendorAvails
    .filter(v =>
      v.Vendor['@Code'].toLowerCase() === vendorId.toLowerCase()
    )[0];

    if (!vendor) {
      return null;
    }

    const selectedVehicle = vendor.VehAvails
    .filter(veh =>
      veh.Vehicle['@Code'].toLowerCase() === carId.toLowerCase()
    )[0];

    if (!selectedVehicle) {
      return null;
    }

    selectedVehicle.Vendor = vendor.Vendor;
    return selectedVehicle;
  })
  .catch(() => null);
};

/* @ngInject */
const onEnter = ($state, vehicleResolve) => { // eslint-disable-line
  if (!vehicleResolve) {
    $state.go('home');
  }
};

/* @ngInject */
export default $stateProvider =>
  $stateProvider
  .state('vehicle', {
    url: '/vehicle/:id',
    resolve: { vehicleResolve },
    onEnter,
    template: vehicleHtml,
    controller: VehicleController,
    controllerAs: 'vehicle'
  });
