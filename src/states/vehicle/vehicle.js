import angular from 'angular';
import uirouter from 'angular-ui-router';
import carsList from 'modules/carsList';

import configRoutes from './configRoutes';

import './vehicle.scss';

export default angular
.module('app.vehicle', [uirouter, carsList])
.config(configRoutes)
.name;
