import angular from 'angular';
import uiRouter from 'angular-ui-router';

import './styles/foundation.scss';

import home from 'states/home'; // eslint-disable-line
import vehicle from 'states/vehicle'; // eslint-disable-line

/* @ngInject */
const config = ($urlRouterProvider, $locationProvider) => {
  $locationProvider.html5Mode(false);
  $urlRouterProvider.otherwise('/');
};

const app = angular
.module('app', [uiRouter, home, vehicle])
.config(config)
.name;

angular.bootstrap(document, [app], { strictDi: true });
