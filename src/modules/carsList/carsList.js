import angular from 'angular';
import carsList from './component/carsList';
import cars from './factory/cars';

export default angular
.module('app.modules.carsList', [])
.factory('carsFactory', cars)
.component('carsList', carsList())
.name;
