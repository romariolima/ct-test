import HomeController from './controller/HomeController';
import home from './view/home.html';

/* @ngInject */
export default $stateProvider =>
  $stateProvider
  .state('home', {
    url: '/',
    template: home,
    controller: HomeController,
    controllerAs: 'home'
  });
